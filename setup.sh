
# Take in the computer type (work or home)
OS='uname'
TYPE=$1

# Settings for installs
VAGRANT_VERSION='2.2.4'
TERRAGRUNT_VERSION='0.18.4'
TERRAFORM_VERSION='0.11.13'

# Setup vimrc
cp .vimrc ~/.vimrc
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall

# Set OS specifics
case OS in
  Linux)
    DISTRO='cat /etc/os-release | grep -e "^NAME" | cut -d= -f2'
    case DISTRO in
      Fedora)
        # Make sure vim is installed
        sudo yum install -y vim
        sudo yum install -y wget
        sudo yum install -y git

        # Install other apps for Gnome
        sudo yum install -y gnome-tweaks

        # Install Development stuff
        sudo yum install -y ranger

        # Make a development folder
        cd ~
        mkdir Development

        if [ TYPE == work ]; then
          # Install Terraform and Terragrunt
          wget https://github.com/gruntwork-io/terragrunt/releases/download/v${TERRAGRUNT_VERSION}/terragrunt_linux_amd64
          mv terragrunt_linux_amd64 terragrunt
          sudo mv terragrunt /usr/local/bin
          wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip
          unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip
          sudo mv terraform /usr/local/bin

          # Install salt
          sudo yum install -y salt-master salt-minion

          # Install work tools
          sudo yum install -y slack,brave

          cd Development

          ## Install Vagrant
          wget https://releases.hashicorp.com/vagrant/${VAGRANT_VERSION}/vagrant_${VAGRANT_VERSION}_linux_amd64.zip
          unzip vagrant_${VAGRANT_VERSION}_linux_amd64.zip
          mv vagrant_${VAGRANT_VERSION}_linux_amd64 vagrant
          mv vagrant /usr/local/bin/vagrant
          vagrant plugin install vagrant-sshfs

          ## Install VirtualBox
          wget https://download.virtualbox.org/virtualbox/6.0.8/VirtualBox-6.0-6.0.8_130520_fedora29-1.x86_64.rpm
          sudo yum install -y VirtualBox-6.0-6.0.8_130520_fedora29-1.x86_64.rpm

          ## Set Up Salt development environment with vagrant
          mkdir salt
          cd salt
          git clone ssh://git@git.econsys.com:2222/salt/vagrant.git
          cp vagrant/Vagrantfile ./
          git clone ssh://git@git.econsys.com:2222/salt/common.git
          git clone ssh://git@git.econsys.com:2222/salt/env.corp.git
          ln -s env.corp env
          
          ## Pull down the Terraform repos
          git pull ssh://git@git.econsys.com:2222/doc/immutable-terraform.git
        else
          # Install Steam
          dnf install -y fedora-workstation-repositories
          dnf install -y steam --enablerepo=rpmfusion-nonfree-steam

        fi
      ;;
    esac
    ;;
  Darwin) 
    ;;
  *)
    echo "For some reason uname was not able to pick up on the operating system on which this is being run.\n"
    echo "This could mean this program is being run on an operating system that is not implemented or not worth the programmers time (Windows... gross).\n"
    ;;
esac
